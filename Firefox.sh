# if the version of FireFox doesn't work or if any issue happing, we try to reinstall FireFox
# We initially install firefox with yum so all the deps are included
yum remove -y firefox

# install latest release FireFox
wget http://ftp.mozilla.org/pub/firefox/releases/63.0b3/linux-x86_64/en-US/firefox-63.0b3.tar.bz2
tar -xvjf firefox-63.0b3.tar.bz2 
mv firefox /usr/local/firefox
ln -s /usr/local/firefox/firefox /usr/local/bin/firefox