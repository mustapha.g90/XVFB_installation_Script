# install deps
yum install -y Xvfb firefox

# download selenium server
wget https://selenium-release.storage.googleapis.com/3.13/selenium-server-standalone-3.13.0.jar

# move it to a known location
mv selenium-server-standalone-2.53.0.jar /opt/selenium-server-standalone.jar

# create a virtual monitor ( 0 is just a referance number to use on Jenkins or any other system)
Xvfb :0 -ac -screen 0 1280x1024x24 &> /dev/null &

# export the environment var for Firefox
echo "export DISPLAY=:0" >> ~/.profile

# run the server
java -jar /opt/selenium-server-standalone.jar &> /dev/null &


echo "Ready to run behat tests"